### Batch process Directionality ###
A simple [Fiji](http://fiji.sc) script written in Groovy to batch process using the 
[Directionality](https://imagej.net/Directionality) plugin. Writes out to the results table appending a line for each 
image matching the specified extension. Files are read in using [Bio 
Formats](https://www.openmicroscopy.org/bio-formats/).

### Acknowledgement ###
Written by [Dave Mason](http://pcwww.liv.ac.uk/~dnmason), from the University of Liverpool [Centre for Cell Imaging](http://cci.liv.ac.uk).
