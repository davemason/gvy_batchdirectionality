#@ File    (label = "Input directory", style = "directory") srcFile
#@ String  (label = "File extension", value=".czi") ext

import ij.IJ
import fiji.analyze.directionality.Directionality_
#@ ResultsTable rt

def main() {
	srcFile.eachFileRecurse {
		name = it.getName()
		if (name.endsWith(ext)) {
			process(it, srcFile)
		}
	}
}

def process(file, src) {
	println "Processing $file"

	// Opening the image
	IJ.run("Bio-Formats Importer", "open=["+file.getAbsolutePath()+"] color_mode=Composite rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT");
	//imp = IJ.openImage(file.getAbsolutePath())
	imp = IJ.getImage()

	// Put your processing steps here

d = new Directionality_()

d.setImagePlus(imp)
//d.setMethod(Directionality_.AnalysisMethod.LOCAL_GRADIENT_ORIENTATION)
d.setMethod(Directionality_.AnalysisMethod.FOURIER_COMPONENTS)
d.setBinNumber(45)
d.computeHistograms()
d.fitHistograms()

result = d.getFitAnalysis()

mainDirection = Math.toDegrees(result[0][0])
dispersion = Math.toDegrees(result[0][1])

rt.incrementCounter()
rt.addLabel(imp.getTitle())
rt.addValue("Main Direction", mainDirection)
rt.addValue("Dispersion", dispersion)
rt.addValue("Amount", result[0][2])
rt.addValue("Goodness", result[0][3])
rt.show("Results")

	// Clean up
	imp.close()
}

main()
println "     ... done"